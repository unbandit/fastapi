from typing import Union

from fastapi import FastAPI
from pydantic import BaseModel
from enum import Enum

app = FastAPI()

class Item(BaseModel):
    name: str
    price: float
    is_offer: Union[bool, None] = None

class ItemEnum(str, Enum):
    me = "me"
    he = "he"

@app.get("/")
async def read_root():
    return {"Hello": "World"}

@app.get("/items/{item_id}")
async def read_item(item_id: ItemEnum | int, q: Union[str, None] = None):
    if item_id == ItemEnum.me:
        return {"item_id": ItemEnum.me, "message": "this is your item"}
    if item_id == ItemEnum.he:
        return {"item_id": ItemEnum.he, "message": "this is his item"}
    return {"item_id": item_id, "q": q}

@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item):
    return {"item_name": item.name, "item_id": item_id}
